CREATE DATABASE movie;
CREATE TABLE movie.ghibli (
    id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
    release_year INT(4),
    title VARCHAR(50),
    director_name VARCHAR(50)
    );
USE movie;
INSERT INTO ghibli (release_year,title,director_name) VALUES ('1984','Nausicaä of the Valley of the Wind','Hayao Miyazaki');